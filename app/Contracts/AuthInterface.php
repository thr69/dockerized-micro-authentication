<?php

namespace App\Contracts;

interface AuthInterface
{
    public function login($request);
    public function logout($request): void;
}
