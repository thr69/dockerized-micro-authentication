<?php

namespace App\Services;

use App\Contracts\AuthInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthService implements AuthInterface
{
    /**
     * @throws ValidationException
     */
    public function login($request)
    {
        $user = User::where('mobile', $request->mobile)->first();

        if (!$user or !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'userName' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $user->createToken('mobile')->plainTextToken;
    }

    public function logout($request): void
    {
        $request->user()->tokens()->delete(); // This will delete all the user's tokens
    }
}
