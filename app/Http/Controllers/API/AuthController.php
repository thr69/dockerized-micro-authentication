<?php

namespace App\Http\Controllers\API;

use App\Helper\ResponseBuilder;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {

        $this->authService = $authService;
    }

    /**
     * @throws ValidationException
     */
    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $token = $this->authService->login($request);

        return resolve(ResponseBuilder::class)
            ->data(['token' => $token])
            ->serverTime()
            ->json();
    }

    public function logout(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->authService->logout($request);

        return resolve(ResponseBuilder::class)
            ->message('Logged out successfully')
            ->json();
    }
}
