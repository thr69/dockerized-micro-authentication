<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\MessageBag;

class ResponseBuilder
{
    private static array $data = [];

    private static MessageBag $errors;

    private static string $message = '';

    private static int $statusCode = 200;
    private static string $serverTime = '';

    public static function message(string $message = ''): self
    {
        self::$message = $message;
        return new static;
    }

    public static function errors(MessageBag $errors = null): self
    {
        self::$errors = $errors;
        return new static;
    }

    public static function statusCode(int $statusCode = 200): self
    {
        self::$statusCode = $statusCode;
        return new static;
    }

    public static function items(array $data = []): self
    {
        self::$data = $data;
        return new static;
    }

    public static function serverTime(): self
    {
        self::$serverTime = Carbon::now()->toDateTimeString();
        return new static;
    }

    public static function json(): JsonResponse
    {
        return response()->json(
            [
                'data' => static::$data,
                'message' => static::$message,
                'errors' => static::$errors,
                'server_time' => static::$serverTime,
            ],
            static::$statusCode
        );
    }
}
